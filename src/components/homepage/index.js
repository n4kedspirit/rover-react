import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { manifestFetchRequest } from '../../actions/rover-manifest-actions.js';

import RoverButton from './../rover-button/index.js';

import { Typography } from 'material-ui';
import './_homepage.scss';

class Homepage extends Component {
  render() {
    return (
      <div className='homepage'>

        <Typography variant='headline' style={{ color: '#FFF', marginTop: 10 }}>Select A Rover</Typography>

        <RoverButton
          roverName='Curiosity'
          img= 'https://www.sg-berjangka.net/wp-content/uploads/2016/07/Robot-NASA-600x400.jpg'
          onClick={() => this.props.manifestFetch('curiosity')}
          link='/rover/curiosity'
          id='rover1'
        />
        
        <RoverButton
          roverName='Opportunity'
          img = 'https://img.purch.com/w/660/aHR0cDovL3d3dy5zcGFjZS5jb20vaW1hZ2VzL2kvMDAwLzA3OS8xMjMvb3JpZ2luYWwvb3BweS5qcGc='
          onClick={() => this.props.manifestFetch('opportunity')}
          link='/rover/opportunity'
          id='rover2'
        />

        <RoverButton
          roverName='Spirit'
          img='https://www.nasa.gov/sites/default/files/styles/full_width_feature/public/images/287330main_image_1215_full.jpg'
          onClick={() => this.props.manifestFetch('opportunity')}
          link='/rover/spirit'
          id='rover3'
        />
      </div>
    );
  }
}

let mapDispatchToProps = (dispatch) => ({
  manifestFetch: (rover) => dispatch(manifestFetchRequest(rover)),
});

export default connect(null, mapDispatchToProps)(Homepage);