import React, { Component } from 'react';
import { connect } from 'react-redux';
import { BrowserRouter, Switch, Route } from 'react-router-dom';

import NavBar from './../navbar';
import Homepage from './../homepage';
import RoverContainer from './../rover-container';

class App extends Component {
  componentWillMount() {
    let localId = localStorage.roverviewId;
  }

  render() {
    return (
      <BrowserRouter>
        <div>
          <Route path='*' component={NavBar} />
          <Route exact path='/' component={Homepage} />
          <Route path='/rover/:roverId' component={RoverContainer} />
        </div>
      </BrowserRouter>
    );
  }
}

let mapDispatchToProps = (dispatch) => ({
  tokenSet: token => dispatch(tokenSet(token)),
});

export default connect(null, mapDispatchToProps)(App);